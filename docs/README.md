| Start | [Prerequisites](setup.md) | [Create Kubernetes Cluster](cluster-setup.md) | [Install Jupyterhub](jupyterhub-setup.md) | [Monitoring](monitoring.md) | [Useful Links](links.md) | [Credits](credits.md) |
| ----- | ------------------------- | --------------------------------------------- | ----------------------------------------- | --------------------------- | ------------------------ | --------------------- |

## Jupyterhub on Kubernetes

Documentation and scripts for users of Nectar Research Cloud to create a Kubernetes cluster with Jupyterhub on their allocation.

This guide will show you how to create a functional Jupyterhub installation in your Nectar allocation using Kubernetes.

To start, to to the [Prerequisites](setup.md) page now
